 //
 //  ViewController.swift
 //  Integration
 //
 //  Created by Click Labs on 3/20/15.
 //  Copyright (c) 2015 Click Labs. All rights reserved.
 //
 
 import UIKit
 
 class ViewController: UIViewController , PTKViewDelegate {
  var payButton: UIBarButtonItem?
  var paymentView: PTKView?
  // generate payment view for stripe on load
  override func viewDidLoad() {
    super.viewDidLoad()
    var nav = self.navigationController?.navigationBar
    paymentView = PTKView(frame: CGRectMake(15, 20, 400, 155))
    paymentView?.center = view.center
    paymentView?.delegate = self
    view.addSubview(paymentView!)
    payButton = UIBarButtonItem(title: "pay", style: UIBarButtonItemStyle.Plain, target: self, action: "createToken")
    payButton!.enabled = false
    navigationItem.rightBarButtonItem = payButton
  }
  //delegate function
  func paymentView(paymentView: PTKView!, withCard card: PTKCard!, isValid valid: Bool) {
    payButton!.enabled = valid
  }
  // validate card and generate token
  func createToken() {
    let card = STPCard()
    card.number = paymentView!.card.number
    card.expMonth = paymentView!.card.expMonth
    card.expYear = paymentView!.card.expYear
    card.cvc = paymentView!.card.cvc
    STPAPIClient.sharedClient().createTokenWithCard(card, completion: { (token: STPToken!, stripeError: NSError!) -> Void in
      if (stripeError != nil){
        println("there is error");
      }else {
        var alert = UIAlertController(title: "Your stripe token is: " + token.tokenId, message: "", preferredStyle: UIAlertControllerStyle.Alert)
        var defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.handleToken(token)
        self.presentViewController(alert, animated: true, completion: nil)
      }
    })
  }
  
  //send token to backend and create charge
  func handleToken(token: STPToken!) {
    
  }
  // func to dismiss keyboard
  override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
    self.view.endEditing(true)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
 }
 
